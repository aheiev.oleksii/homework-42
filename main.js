const saveButton = document.querySelector('#button'),
    table = document.querySelector('table'),
    container = document.querySelector('#cont');

saveButton.addEventListener('click', e => {
    e.preventDefault();
    getDataFromForm();
});

function getDataFromForm() {
    const firstName = document.querySelector('#first-name').value,
        secondName = document.querySelector('#second-name').value,
        birthDate = document.querySelector('#date').value,
        gender = document.querySelector('input[name="gender"]:checked').value,
        city = document.querySelector('#city').value,
        langs = document.querySelector('#item-form'),
        Languages = langs.querySelectorAll('input[name="language"]:checked'),
        adress = document.querySelector('#adress').value;

    const newObj = {
        'First name': firstName,
        'Second name': secondName,
        'Birth date': birthDate,
        Gender: gender,
        City: city,
        Languages: [],
        Adress: adress,
    };

    Languages.forEach(el => {
        newObj.Languages.push(el.value);
    });

    renderForm(newObj);
    table.classList.remove('d-none');
    container.classList.add('d-none');
}

function renderForm(obj) {
    for (let key in obj) {
        const tr = document.createElement('tr'),
            td1 = document.createElement('td'),
            td2 = document.createElement('td');

        td1.innerText = key;
        td2.innerText = obj[key];

        tr.append(td1);
        tr.append(td2);

        table.append(tr);
    }
}
